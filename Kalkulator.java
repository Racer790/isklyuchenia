package net.codejava;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.IOException;

public class Kalkulator {

	private static Scanner scan;

	public static void main(String[] args) throws IOException {
		try {
			System.out.println("Vvedite 2 argumenta");
			scan = new Scanner(System.in);
			int a = scan.nextInt();
			int b = scan.nextInt();
			System.out.println("Viberete jelaemuyu operaciyu: ");
			char ch;
			ch = (char) System.in.read();
			int resultat = 0;
			switch (ch) {
			case '+':
				resultat = a + b;
				break;
			case '-':
				resultat = a - b;
				break;
			case '*':
				resultat = a * b;
				break;
			case '/':
				resultat = a / b;
				break;
			default:  // dobavlen default dlya zadachi
				System.out.println("Nepravil'no vibran znak operacii ");
				break;
			}
			System.out.println("Resultat = " + resultat);
		} catch (InputMismatchException e) { // Oboznachaem isklyuchenie dlya popitki vvoda simvolov, dlya nih
												// vivoditsya oshibka
			System.out.println("Vvedeno nevernoe chislo ili eto simvol");
		} catch (ArithmeticException e) { // Oboznachaem isklyuchenie dlya popitki deleniya na 0, dlya nego toje
											// vivoditsya oshibka
			System.out.println("Popitka deleniya na 0");
		}
	}
}
